#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/atomic.h>
#include <linux/timer.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
static int major;
static wait_queue_head_t wq;
static struct timer_list mytimer;
static int sample_init(void);
static void sample_exit(void);
char timer = 'n';
atomic_t instance_counter;


static void timer_func(unsigned long arg);

static void __exit sample_exit(void)
{
	unregister_chrdev(major, "sample");
	printk("Goodbye, World\n");
}

static ssize_t sample_read(struct file *file, char __user *buf, size_t count, loff_t *ppos)
{
	
	const char* message = "hello world\n";
	int to_copy = count;
	int not_copied;
	
	mytimer.function = timer_func;
	mytimer.data = 0;
	mytimer.expires = jiffies + (1 * HZ);
	add_timer(&mytimer);

	wait_event_interruptible(wq, timer == 'y');
	timer = 'n';

	not_copied = copy_to_user(buf, message, min(strlen(message) * sizeof(char), count));
	return to_copy - not_copied;
}

static int sample_open(struct inode *inode, struct file *file)
{
	int instances = atomic_read(&instance_counter);
	if(instances > 0)
	{
		return -EIO;
	}
	atomic_inc(&instance_counter);

	return 0;
}


static void timer_func(unsigned long arg) {
	timer = 'y';
	wake_up_interruptible(&wq);
}

static int sample_release(struct inode *inode, struct file *file)
{
	del_timer_sync(&mytimer);
	atomic_dec(&instance_counter);
	return 0;
}

static struct file_operations fops = {
	.read = &sample_read,
	.open = &sample_open,
	.release = &sample_release
};



static int __init sample_init(void)
{
	atomic_set(&instance_counter, 0);
	if((major=register_chrdev(0, "sample", &fops)) == 0)
	{
		return -EIO;
	}
	printk("sample: major = %d\n", major);
	
	init_waitqueue_head(&wq);

	init_timer(&mytimer);
	return 0;
}

module_init(sample_init);

module_exit(sample_exit);

