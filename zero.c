#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
static int major;

static int sample_init(void);
static void sample_exit(void);



static void __exit sample_exit(void)
{
	unregister_chrdev(major, "sample");
	printk("Goodbye, World\n");
}

static ssize_t sample_read(struct file *file, char __user *buf, size_t count, loff_t *ppos)
{
	int to_copy = count;
	int not_copied;
	char character = '\0';
	
	not_copied = copy_to_user(buf, &character, sizeof(char));
	return to_copy - not_copied;
}

static struct file_operations fops = {
	.read = &sample_read
};


static int __init sample_init(void)
{
	if((major=register_chrdev(0, "sample", &fops)) == 0)
	{
		return -EIO;
	}
	printk("sample: major = %d\n", major);
	return 0;
}

module_init(sample_init);
module_exit(sample_exit);

