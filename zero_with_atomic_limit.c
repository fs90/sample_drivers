#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/atomic.h>
#include <asm/uaccess.h>
static int major;

static int sample_init(void);
static void sample_exit(void);

atomic_t instance_counter;

static void __exit sample_exit(void)
{
	unregister_chrdev(major, "sample");
	printk("Goodbye, World\n");
}

static ssize_t sample_read(struct file *file, char __user *buf, size_t count, loff_t *ppos)
{
	int to_copy = count;
	int not_copied;
	char character = '\0';
	
	not_copied = copy_to_user(buf, &character, sizeof(char));
	return to_copy - not_copied;
}

static int sample_open(struct inode *inode, struct file *file)
{
	int instances = atomic_read(&instance_counter);
	if(instances > 0)
	{
		return -EIO;
	}
	atomic_inc(&instance_counter);

	return 0;
}


static int sample_release(struct inode *inode, struct file *file)
{
	atomic_dec(&instance_counter);
	return 0;
}

static struct file_operations fops = {
	.read = &sample_read,
	.open = &sample_open,
	.release = &sample_release
};


static int __init sample_init(void)
{
	atomic_set(&instance_counter, 0);
	if((major=register_chrdev(0, "sample", &fops)) == 0)
	{
		return -EIO;
	}
	printk("sample: major = %d\n", major);
	return 0;
}

module_init(sample_init);

module_exit(sample_exit);

